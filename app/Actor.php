<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Actor extends Model
{
    protected $table = 'actor';
    protected $primaryKey = 'id_actor';
    protected $fillable = ['id_actor', 'nombre_actor'];

    public $incrementing = false;
    public $timestamps = false;

    public function pelicula()
    {
        return $this->belongsToMany(Pelicula::class, 'actua', 'id_actor',
            'nro_pelicula');
    }
}
