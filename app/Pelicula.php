<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pelicula extends Model
{
    protected $table = 'pelicula';
    protected $primaryKey = 'nro_catalogo';
    protected $fillable = ['nro_catalogo', 'copias', 'nro_pelicula', 'titulo_pelicula',
        'valor_diarioarriendo', 'costo_pelicula', 'director_pelicula', 'categoria_pelicula'];

    public $incrementing = false;
    public $timestamps = false;

    public function copiaPelicula()
    {
        return $this->hasMany(CopiaPelicula::class, 'nro_catalogo');
    }

    public function sucursal()
    {
        return $this->belongsToMany(Sucursal::class, 'almacena',
            'nro_catalogo', 'nro_sucursal');
    }

    public function actor()
    {
        return $this->belongsToMany(Actor::class, 'actua', 'nro_catalogo',
            'id_actor');
    }
}
