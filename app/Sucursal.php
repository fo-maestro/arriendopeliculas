<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sucursal extends Model
{
    protected $table = 'sucursal';
    protected $primaryKey = 'nro_sucursal';
    protected $fillable = ['nro_sucursal', 'id_direccion', 'nro_telefonicoSU', 'nombre_sucursal'];

    public $incrementing = false;
    public $timestamps = false;

    public function direccion()
    {
        return $this->belongsTo(Direccion::class, 'id_direccion');
    }

    public function empleado()
    {
        return $this->hasMany(Empleado::class, 'nro_sucursal');
    }

    public function pelicula()
    {
        return $this->belongsToMany(Pelicula::class, 'almacena',
            'nro_sucursal', 'nro_catalogo');
    }

    public function cliente()
    {
        return $this->hasMany(Cliente::class, 'nro_cliente');
    }
}
