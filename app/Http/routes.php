<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use Illuminate\Support\Facades\DB;

Route::get('/', function () {
    return view('index')->with('result', DB::select("SELECT * FROM consulta"));
});

Route::resource('clientes', 'ClienteController');
Route::resource('arriendos', 'ArriendaController');
Route::resource('empleados', 'EmpleadoController');
Route::resource('sucursales', 'SucursalController');
Route::resource('peliculas', 'PeliculaController');

Route::get('/consultas/1', function (){
    return view('consultas.consulta1')->with('result', DB::select("select distinct nombrecompleto_cliente from cliente c, arrienda a, copia_pelicula cp, pelicula p
where c.nro_cliente=a.nro_cliente
      AND a.correlativo=cp.correlativo
      AND cp.nro_catalogo=p.nro_catalogo
      AND p.categoria_pelicula='adultos'"));
});

Route::get('/consultas/2', function (){
    return view('consultas.consulta2')->with('result', DB::select("select nro_catalogo, titulo_pelicula
from (select nro_cliente, nombre_estado
	from (select nro_sucursal, nombre_estado 
		from ciudad c, direccion d, sucursal s
		where d.id_ciudad=c.id_ciudad
		AND d.id_direccion=s.id_direccion) as tab, cliente cli
	where tab.nro_sucursal = cli.nro_sucursal) as tab1,
(select nro_cliente, p.nro_catalogo, titulo_pelicula
	from pelicula p, copia_pelicula cp, arrienda ar
	where p.nro_catalogo = cp.nro_catalogo
	AND ar.correlativo = cp.correlativo) as tab2
where tab1.nro_cliente = tab2.nro_cliente
and tab1.nombre_estado = 'NULL'"));
});

Route::get('consultas/3', function (){
    return view('consultas.consulta3')->with('result', DB::select("select  s.nro_sucursal, count(*) as empleado,
avg (salario_empleado) as salario
from sucursal s, empleado e
where s.nro_sucursal=e.nro_sucursal
group by s.nro_sucursal
having count(*)>10"));
});

Route::get('consultas/4', function (){
    return view('consultas.consulta4')->with('result', DB::select("select director_pelicula, count(director_pelicula) as categoria_pelicula
from pelicula
group by director_pelicula
having count(director_pelicula)>2 or count(director_pelicula)=2"));
});

Route::get('consultas/5', function (){
    return view('consultas.consulta5')->with('result', DB::select("select titulo_pelicula, categoria_pelicula
from pelicula p, copia_pelicula cp, arrienda ar
where p.nro_catalogo = cp.nro_catalogo
and cp.correlativo = ar.correlativo
and EXTRACT(MONTH FROM fecha_arriendo) <> 5
group by categoria_pelicula, titulo_pelicula"));
});

Route::get('consultas/6', function (){
    return view('consultas.consulta6')->with('result', DB::select("SELECT nombre_cliente, cantidad
FROM consulta
WHERE cantidad=( 
SELECT MAX( cantidad )  FROM consulta)"));
});