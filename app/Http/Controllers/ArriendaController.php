<?php

namespace App\Http\Controllers;

use App\Cliente;
use App\CopiaPelicula;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;

class ArriendaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return View::make('arriendos.index')->with('arriendos', DB::table('arrienda')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View::make('arriendos.create', [
            'clientes' => Cliente::all(),
            'correlativo' => CopiaPelicula::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'devolucion' => ['required', 'date', 'after:today']
        ]);

        Cliente::find(Input::get('cliente'))->copia_pelicula()->attach(Input::get('pelicula'), [
            'fecha_arriendo' => Carbon::now()->toDateString(),
            'fecha_devolucion' => date('Y-m-d', strtotime(Input::get('devolucion')))
        ]);

        return redirect()->to('arriendos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect()->to('arriendos');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arriendo = DB::table('arrienda')->where('nro_arriendo', $id)->first();

        return View::make('arriendos.edit', [
            'arriendo' => $arriendo,
            'clientes' => Cliente::all(),
            'correlativo' => CopiaPelicula::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'devolucion' => ['required', 'date', 'after:today']
        ]);

        DB::table('arrienda')->where('nro_arriendo', $id)->update([
            'nro_cliente' => Input::get('cliente'),
            'correlativo' => Input::get('pelicula'),
            'fecha_devolucion' => date('Y-m-d', strtotime(Input::get('devolucion')))
        ]);

        return redirect()->to('arriendos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('arrienda')->where('nro_arriendo', $id)->delete();

        return redirect()->to('arriendos');
    }
}
