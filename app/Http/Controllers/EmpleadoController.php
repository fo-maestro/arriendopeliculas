<?php

namespace App\Http\Controllers;

use App\Empleado;
use App\Sucursal;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;

class EmpleadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return View::make('empleados.index')->with('empleados', Empleado::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View::make('empleados.create')->with('sucursal', Sucursal::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nombre' => 'required',
            'salario' => ['required', 'numeric', 'min:0'],
            'cargo' => 'required'
        ]);

        $empleado = new Empleado();
        $empleado->nro_sucursal = Input::get('sucursal');
        $empleado->nombre_empleado = Input::get('nombre');
        $empleado->salario_empleado = Input::get('salario');
        $empleado->cargo_empleado = Input::get('cargo');

        $empleado->save();

        return redirect()->to('empleados');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect()->to('empleados');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return View::make('empleados.edit', [
            'empleado' => Empleado::find($id),
            'sucursales' => Sucursal::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nombre' => 'required',
            'salario' => ['required', 'numeric'],
            'cargo' => 'required'
        ]);

        $empleado = Empleado::find($id);
        $empleado->nro_sucursal = Input::get('sucursal');
        $empleado->nombre_empleado = Input::get('nombre');
        $empleado->salario_empleado = Input::get('salario');
        $empleado->cargo_empleado = Input::get('cargo');

        $empleado->save();

        return redirect()->to('empleados');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Empleado::find($id)->delete();

        return redirect()->to('empleados');
    }
}
