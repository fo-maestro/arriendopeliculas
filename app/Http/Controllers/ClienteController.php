<?php

namespace App\Http\Controllers;

use App\Cliente;
use App\Sucursal;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return View::make('clientes.index')->with('clientes', Cliente::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View::make('clientes.create')->with('sucursales', Sucursal::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nombre' => ['required']
        ]);

        $cliente = new Cliente();
        $cliente->nro_sucursal = Input::get('sucursal');
        $cliente->fecha_registro = Carbon::now()->toDateString();
        $cliente->nombrecompleto_cliente = Input::get('nombre');
        $cliente->save();

        return redirect()->to('clientes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect()->to('clientes');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return View::make('clientes.edit', [
            'cliente' => Cliente::find($id), 'sucursales' => Sucursal::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nombre' => 'required'
        ]);

        $cliente = Cliente::find($id);
        $cliente->nro_sucursal = Input::get('sucursal');
        $cliente->nombrecompleto_cliente = Input::get('nombre');
        $cliente->save();

        return redirect()->to('clientes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Cliente::find($id)->delete();
        } catch (QueryException $ex) {
            return redirect()->back()->withErrors('No se pudo eliminar, Violacion de llave foranea');
        }

        return redirect()->to('clientes');
    }
}
