<?php

namespace App\Http\Controllers;

use App\Pelicula;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;

class PeliculaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return View::make('peliculas.index')->with('peliculas', Pelicula::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View::make('peliculas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'catalogo' => ['required', 'numeric', 'min:0'],
            'copias' => ['required', 'numeric', 'min:0'],
            'numero' => ['required', 'numeric', 'min:0'],
            'titulo' => 'required',
            'arriendo' => ['required', 'numeric', 'min:0'],
            'costo' => ['required', 'numeric', 'min:0'],
            'director' => 'required',
            'categoria' => 'required'
        ]);

        $pelicula = new Pelicula();

        $pelicula->nro_catalogo = Input::get('catalogo');
        $pelicula->copias = Input::get('copias');
        $pelicula->nro_pelicula = Input::get('numero');
        $pelicula->titulo_pelicula = Input::get('titulo');
        $pelicula->valor_diarioarriendo = Input::get('arriendo');
        $pelicula->costo_pelicula = Input::get('costo');
        $pelicula->director_pelicula = Input::get('director');
        $pelicula->categoria_pelicula = Input::get('categoria');

        try {
            $pelicula->save();
        } catch (QueryException $ex) {
            return redirect()->back()->withErrors('No se pudo ingresar, llave primaria duplicada');
        }

        return redirect()->to('peliculas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect()->to('peliculas');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return View::make('peliculas.edit')->with('pelicula', Pelicula::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'copias' => ['required', 'numeric', 'min:0'],
            'numero' => ['required', 'numeric', 'min:0'],
            'titulo' => 'required',
            'arriendo' => ['required', 'numeric', 'min:0'],
            'costo' => ['required', 'numeric', 'min:0'],
            'director' => 'required',
            'categoria' => 'required'
        ]);

        $pelicula = Pelicula::find($id);

        $pelicula->copias = Input::get('copias');
        $pelicula->nro_pelicula = Input::get('numero');
        $pelicula->titulo_pelicula = Input::get('titulo');
        $pelicula->valor_diarioarriendo = Input::get('arriendo');
        $pelicula->costo_pelicula = Input::get('costo');
        $pelicula->director_pelicula = Input::get('director');
        $pelicula->categoria_pelicula = Input::get('categoria');

        $pelicula->save();

        return redirect()->to('peliculas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Pelicula::find($id)->delete();
        } catch (QueryException $ex) {
            return redirect()->back()->withErrors('No se pudo eliminar, Violacion de llave foranea');
        }

        return redirect()->to('peliculas');
    }
}
