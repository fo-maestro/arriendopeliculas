<?php

namespace App\Http\Controllers;

use App\Direccion;
use App\Sucursal;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;

class SucursalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return View::make('sucursales.index')->with('sucursales', Sucursal::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View::make('sucursales.create')->with('direcciones', Direccion::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'numero' => ['required', 'numeric', 'min:0'],
            'telefono' => ['required', 'numeric', 'min:100000000', 'max:999999999'],
            'nombre' => 'required'
        ]);

        $sucursal = new Sucursal();

        $sucursal->nro_sucursal = Input::get('numero');
        $sucursal->id_direccion = Input::get('direccion');
        $sucursal->nro_telefonicoSu = Input::get('telefono');
        $sucursal->nombre_sucursal = Input::get('nombre');

        $sucursal->save();

        return redirect()->to('sucursales');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect()->to('sucursales');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return View::make('sucursales.edit', [
            'sucursal' => Sucursal::find($id),
            'direcciones' => Direccion::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'telefono' => ['required', 'numeric', 'min:100000000', 'max:999999999'],
            'nombre' => 'required'
        ]);

        $sucursal = Sucursal::find($id);
        $sucursal->id_direccion = Input::get('direccion');
        $sucursal->nro_telefonicoSU = Input::get('telefono');
        $sucursal->nombre_sucursal = Input::get('nombre');

        $sucursal->save();

        return redirect()->to('sucursales');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Sucursal::find($id)->delete();
        } catch (QueryException $ex) {
            return redirect()->back()->withErrors('No se pudo eliminar, Violacion de llave foranea');
        }

        return redirect()->to('sucursales');
    }
}
