<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CopiaPelicula extends Model
{
    protected $table = 'copia_pelicula';
    protected $primaryKey = 'correlativo';
    protected $fillable = ['correlativo', 'nro_catalogo', 'estado_copiapelicula'];

    public $incrementing = false;
    public $timestamps = false;

    public function pelicula()
    {
        return $this->belongsTo(Pelicula::class, 'nro_catalogo');
    }

    public function cliente()
    {
        return $this->belongsToMany(Cliente::class, 'arrienda',
            'correlativo', 'nro_cliente')
            ->withPivot('fecha_arriendo', 'fecha_devolucion');
    }
}
