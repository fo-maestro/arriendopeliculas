<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ciudad extends Model
{
    protected $table = 'ciudad';
    protected $primaryKey = 'id_ciudad';
    protected $fillable = ['id_ciudad', 'nombre_ciudad', 'nombre_estado'];

    public $incrementing = false;
    public $timestamps = false;

    public function direccion()
    {
        return $this->hasMany(Direccion::class, 'id_ciudad');
    }
}
