<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Direccion extends Model
{
    protected $table = 'direccion';
    protected $primaryKey = 'id_direccion';
    protected $fillable = ['id_direccion', 'id_ciudad', 'calle', 'codigo_postal'];

    public $incrementing = false;
    public $timestamps = false;

    public function ciudad()
    {
        return $this->belongsTo(Ciudad::class, 'id_ciudad');
    }

    public function sucursal()
    {
        return $this->hasOne(Sucursal::class, 'nro_sucursal');
    }
}
