<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $table = 'cliente';
    protected $primaryKey = 'nro_cliente';
    protected $fillable = ['nro_sucursal', 'nombrecompleto_cliente', 'fecha_registro'];

    public $timestamps = false;

    public function sucursal()
    {
        return $this->belongsTo(Sucursal::class, 'nro_sucursal');
    }

    public function copia_pelicula()
    {
        return $this->belongsToMany(CopiaPelicula::class, 'arrienda',
            'nro_cliente', 'correlativo')
            ->withPivot('fecha_arriendo', 'fecha_devolucion');
    }
}
