<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empleado extends Model
{
    protected $table = 'empleado';
    protected $primaryKey = 'nro_empleado';
    protected $fillable = ['nro_sucursal', 'nombre_empleado', 'salario_empleado', 'cargo_empleado'];

    public $timestamps = false;

    public function sucursal()
    {
        return $this->belongsTo(Sucursal::class, 'nro_sucursal');
    }
}
