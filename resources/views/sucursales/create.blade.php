@extends('layout')
@section('content')
    @include('partials/error')
    <div class="row">
        <div class="col-md-offset-4 col-md-4 col-md-offset-4">
            <h1>Insertar Sucursal</h1>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-offset-3 col-md-6 col-md-offset-3">
            <form method="post" action="{{ url('sucursales') }}" class="form-horizontal">
                {!! csrf_field() !!}
                <div class="form-group">
                    <label class="control-label col-sm-2" for="numero">Numero:</label>
                    <div class="col-sm-10">
                        <input id="numero" type="text" name="numero" class="form-control" value="{{ old('numero') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="direccion">Direccion:</label>
                    <div class="col-sm-10">
                        <select id="direccion" name="direccion" class="form-control">
                            @foreach($direcciones as $value)
                                <option value="{{ $value->id_direccion }}">{{ $value->calle }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="telefono">Telefono:</label>
                    <div class="col-sm-10">
                        <input id="telefono" type="text" name="telefono" class="form-control" value="{{ old('telefono') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="nombre">Nombre:</label>
                    <div class="col-sm-10">
                        <input id="nombre" type="text" name="nombre" class="form-control" value="{{ old('nombre') }}">
                    </div>
                </div>
                <button type="submit" class="btn btn-success">Crear</button>
                <div class="form-group col-md-6"></div>
                <div class="form-group">
                    <div class="form-group"></div>
                </div>
            </form>
        </div>
    </div>
@endsection