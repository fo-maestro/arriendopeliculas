@extends('layout')
@section('content')
    @include('partials/error')
    <div class="row">
        <div class="col-md-offset-4 col-md-4 col-md-offset-4">
            <h1>Sucursales</h1>
        </div>
    </div>
    <a href="{{ url('sucursales/create') }}" class="btn pull-right btn-primary">Insertar</a>
    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <td>Numero</td>
                <td>Direccion</td>
                <td>Telefono</td>
                <td>Nombre</td>
                <td>Acciones</td>
            </tr>
        </thead>
        <tbody>
            @foreach($sucursales as $value)
                <tr>
                    <td>{{ $value->nro_sucursal }}</td>
                    <td>{{ \App\Direccion::find($value->id_direccion)->calle }}</td>
                    <td>{{ $value->nro_telefonicoSU }}</td>
                    <td>{{ $value->nombre_sucursal }}</td>
                    <td>
                        <div class="btn-group">
                            <a href="{{ url('sucursales/'.$value->nro_sucursal.'/edit') }}" class="btn btn-small btn-success">Editar</a>
                            <form method="POST" action="{{ url('sucursales/'.$value->nro_sucursal) }}" class="pull-right">
                                <input name="_method" type="hidden" value="DELETE">
                                {!! csrf_field() !!}
                                <button type="submit" class="btn btn-danger">Eliminar</button>
                            </form>
                        </div>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection