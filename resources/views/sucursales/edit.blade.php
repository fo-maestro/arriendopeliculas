@extends('layout')
@section('content')
    @include('partials/error')
    <div class="row">
        <div class="col-md-offset-4 col-md-8">
            <h1>Editar Sucursal: {{ $sucursal->nro_sucursal }}</h1>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-offset-3 col-md-6 col-md-offset-3">
            <form method="post" action="{{ url('sucursales/'.$sucursal->nro_sucursal) }}" class="form-horizontal">
                <input name="_method" type="hidden" value="PUT">
                {!! csrf_field() !!}
                <div class="form-group">
                    <label class="control-label col-sm-2" for="direccion">Direccion:</label>
                    <div class="col-sm-10">
                        <select id="direccion" name="direccion" class="form-control">
                            @foreach($direcciones as $value)
                                <option @if($sucursal->id_direccion == $value->id_direccion) selected="selected" @endif value="{{ $value->id_direccion }}">{{ $value->calle }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="telefono">Telefono:</label>
                    <div class="col-sm-10">
                        <input id="telefono" type="text" name="telefono" class="form-control" value="{{ $sucursal->nro_telefonicoSU }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="nombre">Nombre:</label>
                    <div class="col-sm-10">
                        <input id="nombre" type="text" name="nombre" class="form-control" value="{{ $sucursal->nombre_sucursal }}">
                    </div>
                </div>
                <button type="submit" class="btn btn-success">Editar</button>
                <div class="form-group col-md-6"></div>
                <div class="form-group">
                    <div class="form-group"></div>
                </div>
            </form>
        </div>
    </div>
@endsection