@extends('layout')
@section('content')
    @include('lateral')
    <div class="col-md-9">
        <div class="row">
            <div class="col-md-offset-4 col-md-4 col-md-offset-4">
                <h1>Consulta 3</h1>
            </div>
        </div>
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <td>Sucursal</td>
                <td>Empleado</td>
                <td>Salario</td>
            </tr>
            </thead>
            <tbody>
            @foreach($result as $value)
                <tr>
                    <td>{{ $value->nro_sucursal }}</td>
                    <td>{{ $value->empleado }}</td>
                    <td>{{ $value->salario }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection