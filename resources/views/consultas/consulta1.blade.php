@extends('layout')
@section('content')
    @include('lateral')
    <div class="col-md-9">
        <div class="row">
            <div class="col-md-offset-4 col-md-4 col-md-offset-4">
                <h1>Consulta 1</h1>
            </div>
        </div>
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <td>Nombre Completo</td>
            </tr>
            </thead>
            <tbody>
                @foreach($result as $value)
                    <tr>
                        <td>{{ $value->nombrecompleto_cliente }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection