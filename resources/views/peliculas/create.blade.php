@extends('layout')
@section('content')
    @include('partials/error')
    <div class="row">
        <div class="col-md-offset-4 col-md-4 col-md-offset-4">
            <h1>Insertar Pelicula</h1>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-offset-3 col-md-6 col-md-offset-3">
            <form method="post" action="{{ url('peliculas') }}" class="form-horizontal">
                {!! csrf_field() !!}
                <div class="form-group">
                    <label class="control-label col-sm-2" for="catalogo">Catalogo:</label>
                    <div class="col-sm-10">
                        <input id="catalogo" type="text" name="catalogo" class="form-control" value="{{ old('catalogo') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="copias">Copias:</label>
                    <div class="col-sm-10">
                        <input id="copias" type="text" name="copias" class="form-control" value="{{ old('copias') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="numero">Numero:</label>
                    <div class="col-sm-10">
                        <input id="numero" type="text" name="numero" class="form-control" value="{{ old('numero') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="titulo">Titulo:</label>
                    <div class="col-sm-10">
                        <input id="titulo" type="text" name="titulo" class="form-control" value="{{ old('titulo') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="arriendo">Arriendo:</label>
                    <div class="col-sm-10">
                        <input id="arriendo" type="text" name="arriendo" class="form-control" value="{{ old('arriendo') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="costo">Costo:</label>
                    <div class="col-sm-10">
                        <input id="costo" type="text" name="costo" class="form-control" value="{{ old('costo') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="director">Director:</label>
                    <div class="col-sm-10">
                        <input id="director" type="text" name="director" class="form-control" value="{{ old('director') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="categoria">Categoria:</label>
                    <div class="col-sm-10">
                        <input id="categoria" type="text" name="categoria" class="form-control" value="{{ old('categoria') }}">
                    </div>
                </div>
                <button type="submit" class="btn btn-success">Crear</button>
                <div class="form-group col-md-6"></div>
                <div class="form-group">
                    <div class="form-group"></div>
                </div>
            </form>
        </div>
    </div>
@endsection