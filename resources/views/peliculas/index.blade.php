@extends('layout')
@section('content')
    <div class="row">
        <div class="col-md-offset-4 col-md-4 col-md-offset-4">
            <h1>Peliculas</h1>
        </div>
    </div>
    <a href="{{ url('peliculas/create') }}" class="btn pull-right btn-primary">Insertar</a>
    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <td>Catalogo</td>
                <td>Copias</td>
                <td>Numero</td>
                <td>Titulo</td>
                <td>Arriendo</td>
                <td>Costo</td>
                <td>Director</td>
                <td>Categoria</td>
                <td>Acciones</td>
            </tr>
        </thead>
        <tbody>
            @foreach($peliculas as $value)
                <tr>
                    <td>{{ $value->nro_catalogo }}</td>
                    <td>{{ $value->copias }}</td>
                    <td>{{ $value->nro_pelicula }}</td>
                    <td>{{ $value->titulo_pelicula }}</td>
                    <td>{{ $value->valor_diarioarriendo }}</td>
                    <td>{{ $value->costo_pelicula }}</td>
                    <td>{{ $value->director_pelicula }}</td>
                    <td>{{ $value->categoria_pelicula }}</td>
                    <td>
                        <div class="btn-group">
                            <a href="{{ url('peliculas/'.$value->nro_catalogo.'/edit') }}" class="btn btn-small btn-success">Editar</a>
                            <form method="POST" action="{{ url('peliculas/'.$value->nro_catalogo) }}" class="pull-right">
                                <input name="_method" type="hidden" value="DELETE">
                                {!! csrf_field() !!}
                                <button type="submit" class="btn btn-danger">Eliminar</button>
                            </form>
                        </div>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection