@extends('layout')
@section('content')
    @include('partials/error')
    <div class="row">
        <div class="col-md-offset-4 col-md-4 col-md-offset-4">
            <h1>Insertar Arriendo</h1>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-offset-3 col-md-6 col-md-offset-3">
            <form method="post" action="{{ url('arriendos') }}" class="form-horizontal">
                {!! csrf_field() !!}
                <div class="form-group">
                    <label class="control-label col-sm-2" for="cliente">Cliente:</label>
                    <div class="col-sm-10">
                        <select id="cliente" name="cliente" class="form-control">
                            @foreach($clientes as $value)
                                <option value="{{ $value->nro_cliente }}">{{ $value->nombrecompleto_cliente}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="pelicula">Pelicula:</label>
                    <div class="col-sm-10">
                        <select id="pelicula" name="pelicula" class="form-control">
                            @foreach($correlativo as $value)
                                <option value="{{ $value->correlativo }}">{{ \App\Pelicula::find($value->nro_catalogo)->titulo_pelicula }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="devolucion">Devolucion:</label>
                    <div class="col-sm-10">
                        <input id="devolucion" type="text" name="devolucion" class="form-control" placeholder="dd-mm-yyyy">
                    </div>
                </div>
                <button type="submit" class="btn btn-success">Crear</button>
                <div class="form-group col-md-6"></div>
                <div class="form-group">
                    <div class="form-group"></div>
                </div>
            </form>
        </div>
    </div>
@endsection