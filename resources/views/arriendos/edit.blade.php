@extends('layout')
@section('content')
    @include('partials/error')
    <div class="row">
        <div class="col-md-offset-2 col-md-10">
            <h1>Editar Arriendo: {{ $arriendo->nro_arriendo }}</h1>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-offset-3 col-md-6 col-md-offset-3">
            <form method="post" action="{{ url('arriendos/'.$arriendo->nro_arriendo) }}" class="form-horizontal">
                <input name="_method" type="hidden" value="PUT">
                {!! csrf_field() !!}
                <div class="form-group">
                    <label class="control-label col-sm-2" for="cliente">Cliente:</label>
                    <div class="col-sm-10">
                        <select id="cliente" name="cliente" class="form-control">
                            @foreach($clientes as $value)
                                <option @if($arriendo->nro_cliente == $value->nro_cliente) selected="selected" @endif value="{{ $value->nro_cliente }}">{{ $value->nombrecompleto_cliente }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="pelicula">Pelicula:</label>
                    <div class="col-sm-10">
                        <select id="pelicula" name="pelicula" class="form-control">
                            @foreach($correlativo as $value)
                                <option @if($arriendo->correlativo == $value->correlativo) selected="selected" @endif value="{{ $value->correlativo }}">{{ \App\Pelicula::find($value->nro_catalogo)->titulo_pelicula }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="devolucion">Devolucion:</label>
                    <div class="col-sm-10">
                        <input id="devolucion" type="text" name="devolucion" class="form-control" placeholder="dd-mm-yyyy" value="{{ date('d-m-Y', strtotime($arriendo->fecha_devolucion)) }}">
                    </div>
                </div>
                <button type="submit" class="btn btn-success">Editar</button>
                <div class="form-group col-md-6"></div>
                <div class="form-group">
                    <div class="form-group"></div>
                </div>
            </form>
        </div>
    </div>
@endsection