@extends('layout')
@section('content')
    <div class="row">
        <div class="col-md-offset-4 col-md-4 col-md-offset-4">
            <h1>Arriendos</h1>
        </div>
    </div>
    <a href="{{ url('arriendos/create') }}" class="btn pull-right btn-primary">Insertar</a>
    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <td>Arriendo</td>
                <td>Cliente</td>
                <td>Pelicula</td>
                <td>Fecha Arriendo</td>
                <td>Fecha Devolucion</td>
                <td>Acciones</td>
            </tr>
        </thead>
        <tbody>
            @foreach($arriendos as $value)
                <tr>
                    <td>{{ $value->nro_arriendo }}</td>
                    <td>{{ \App\Cliente::find($value->nro_cliente)->nombrecompleto_cliente }}</td>
                    <td>{{ \App\Pelicula::find(\App\CopiaPelicula::find($value->correlativo)->nro_catalogo)->titulo_pelicula }}</td>
                    <td>{{ date('d-m-Y', strtotime($value->fecha_arriendo)) }}</td>
                    <td>{{ date('d-m-Y', strtotime($value->fecha_devolucion)) }}</td>
                    <td>
                        <div class="btn-group">
                            <a href="{{ url('arriendos/'.$value->nro_arriendo.'/edit') }}" class="btn btn-small btn-success">Editar</a>
                            <form method="POST" action="{{ url('arriendos/'.$value->nro_arriendo) }}" class="pull-right">
                                <input name="_method" type="hidden" value="DELETE">
                                {!! csrf_field() !!}
                                <button type="submit" class="btn btn-danger">Eliminar</button>
                            </form>
                        </div>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection