@extends('layout')
@section('content')
    @include('partials/error')
    <div class="row">
        <div class="col-md-offset-4 col-md-4 col-md-offset-4">
            <h1>Insertar Empleado</h1>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-offset-3 col-md-6 col-md-offset-3">
            <form method="post" action="{{ url('empleados') }}" class="form-horizontal">
                {!! csrf_field() !!}
                <div class="form-group">
                    <label class="control-label col-sm-2" for="sucursal">Sucursal:</label>
                    <div class="col-sm-10">
                        <select id="sucursal" name="sucursal" class="form-control">
                            @foreach($sucursal as $value)
                                <option value="{{ $value->nro_sucursal }}">{{ $value->nombre_sucursal}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="nombre">Nombre:</label>
                    <div class="col-sm-10">
                        <input id="nombre" type="text" name="nombre" class="form-control" value="{{ old('nombre') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="salario">Salario:</label>
                    <div class="col-sm-10">
                        <input id="salario" type="text" name="salario" class="form-control" value="{{ old('salario') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="cargo">Cargo:</label>
                    <div class="col-sm-10">
                        <input id="cargo" type="text" name="cargo" class="form-control" value="{{ old('cargo') }}">
                    </div>
                </div>
                <button type="submit" class="btn btn-success">Crear</button>
                <div class="form-group col-md-6"></div>
                <div class="form-group">
                    <div class="form-group"></div>
                </div>
            </form>
        </div>
    </div>
@endsection