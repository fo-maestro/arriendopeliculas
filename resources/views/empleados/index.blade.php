@extends('layout')
@section('content')
    <div class="row">
        <div class="col-md-offset-4 col-md-4 col-md-offset-4">
            <h1>Empleados</h1>
        </div>
    </div>
    <a href="{{ url('empleados/create') }}" class="btn pull-right btn-primary">Insertar</a>
    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <td>Empleado</td>
                <td>Sucursal</td>
                <td>Nombre</td>
                <td>Salario</td>
                <td>Cargo</td>
                <td>Acciones</td>
            </tr>
        </thead>
        <tbody>
            @foreach($empleados as $value)
                <tr>
                    <td>{{ $value->nro_empleado }}</td>
                    <td>{{ \App\Sucursal::find($value->nro_sucursal)->nombre_sucursal }}</td>
                    <td>{{ $value->nombre_empleado }}</td>
                    <td>{{ $value->salario_empleado }}</td>
                    <td>{{ $value->cargo_empleado }}</td>
                    <td>
                        <div class="btn-group">
                            <a href="{{ url('empleados/'.$value->nro_empleado.'/edit') }}" class="btn btn-small btn-success">Editar</a>
                            <form method="POST" action="{{ url('empleados/'.$value->nro_empleado) }}" class="pull-right">
                                <input name="_method" type="hidden" value="DELETE">
                                {!! csrf_field() !!}
                                <button type="submit" class="btn btn-danger">Eliminar</button>
                            </form>
                        </div>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection