@extends('layout')
@section('content')
    @include('lateral')
    <div class="col-md-9">
        <div class="row">
            <div class="col-md-offset-4 col-md-4 col-md-offset-4">
                <h1>Vista</h1>
            </div>
        </div>
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <td>Cliente</td>
                <td>Nombre</td>
                <td>Cantidad</td>
            </tr>
            </thead>
            <tbody>
            @foreach($result as $value)
                <tr>
                    <td>{{ $value->codigo_cliente }}</td>
                    <td>{{ $value->nombre_cliente }}</td>
                    <td>{{ $value->cantidad }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection