@extends('layout')
@section('content')
    @include('partials/error')
    <div class="row">
        <div class="col-md-offset-2 col-md-10">
            <h1>Editar Cliente: {{ $cliente->nombrecompleto_cliente }}</h1>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-offset-3 col-md-5 col-md-offset-4">
            <form method="post" action="{{ url('clientes/'.$cliente->nro_cliente) }}" class="form-horizontal">
                <input name="_method" type="hidden" value="PUT">
                {!! csrf_field() !!}
                <div class="form-group">
                    <label class="control-label col-sm-2" for="sucursal">Sucursal:</label>
                    <div class="col-sm-10">
                        <select id="sucursal" name="sucursal" class="form-control">
                            @foreach($sucursales as $value)
                                <option @if($cliente->nro_sucursal == $value->nro_sucursal) selected="selected" @endif value="{{ $value->nro_sucursal }}">{{ $value->nombre_sucursal }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="nombre">Nombre:</label>
                    <div class="col-sm-10">
                        <input id="nombre" name="nombre" type="text" class="form-control" value="{{ $cliente->nombrecompleto_cliente }}">
                    </div>
                </div>
                <button type="submit" class="btn btn-success">Editar</button>
                <div class="form-group col-md-6"></div>
                <div class="form-group">
                    <div class="form-group"></div>

                </div>
            </form>
        </div>
    </div>
@endsection