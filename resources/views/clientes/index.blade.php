@extends('layout')
@section('content')
    @include('partials/error')
    <div class="row">
        <div class="col-md-offset-4 col-md-4 col-md-offset-4">
            <h1>Clientes</h1>
        </div>
    </div>
    <a href="{{ url('clientes/create') }}" class="btn pull-right btn-primary">Insertar</a>
    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <td>Cliente</td>
                <td>Sucursal</td>
                <td>Registro</td>
                <td>Nombre</td>
                <td>Acciones</td>
            </tr>
        </thead>
        <tbody>
            @foreach($clientes as $value)
                <tr>
                    <td>{{ $value->nro_cliente }}</td>
                    <td>{{ \App\Sucursal::find($value->nro_sucursal)->nombre_sucursal }}</td>
                    <td>{{ date('d-m-Y', strtotime($value->fecha_registro)) }}</td>
                    <td>{{ $value->nombrecompleto_cliente }}</td>
                    <td>
                        <div class="btn-group">
                            <a href="{{ url('clientes/'.$value->nro_cliente.'/edit') }}" class="btn btn-small btn-success">Editar</a>
                            <form method="POST" action="{{ url('clientes/'.$value->nro_cliente) }}" class="pull-right">
                                <input name="_method" type="hidden" value="DELETE">
                                {!! csrf_field() !!}
                                <button type="submit" class="btn btn-danger">Eliminar</button>
                            </form>
                        </div>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection