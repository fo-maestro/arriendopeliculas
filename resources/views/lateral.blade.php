<div class="col-md-3">
    <p class="lead">Consultas</p>
    <div class="list-group">
        <a href="{{ url('consultas/1') }}" class="list-group-item">Consulta 1</a>
        <a href="{{ url('consultas/2') }}" class="list-group-item">Consulta 2</a>
        <a href="{{ url('consultas/3') }}" class="list-group-item">Consulta 3</a>
        <a href="{{ url('consultas/4') }}" class="list-group-item">Consulta 4</a>
        <a href="{{ url('consultas/5') }}" class="list-group-item">Consulta 5</a>
        <a href="{{ url('consultas/6') }}" class="list-group-item">Consulta 6</a>
    </div>
</div>