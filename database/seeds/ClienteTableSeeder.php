<?php

use App\Cliente;
use Illuminate\Database\Seeder;

class ClienteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Cliente::create([
            'nro_sucursal' => 123,
            'nombrecompleto_cliente' => 'Fabian Andres Gonzales Pereira',
            'fecha_registro' => '2016-02-01'
        ]);

        Cliente::create([
            'nro_sucursal' => 123,
            'nombrecompleto_cliente' => 'Marcia Odette Sandoval',
            'fecha_registro' => '2016-03-03'
        ]);

        Cliente::create([
            'nro_sucursal' => 124,
            'nombrecompleto_cliente' => 'Luis Rodriguez Hernandez',
            'fecha_registro' => '2016-03-03'
        ]);

        Cliente::create([
            'nro_sucursal' => 123,
            'nombrecompleto_cliente' => 'Guillermo Andres Pereira Oses',
            'fecha_registro' => '2016-04-11'
        ]);

        Cliente::create([
            'nro_sucursal' => 125,
            'nombrecompleto_cliente' => 'Julia Martinez Sandoval',
            'fecha_registro' => '2016-04-11'
        ]);

        Cliente::create([
            'nro_sucursal' => 125,
            'nombrecompleto_cliente' => 'Marcela Rodriguez Hernandez',
            'fecha_registro' => '2016-05-17'
        ]);

        Cliente::create([
            'nro_sucursal' => 123,
            'nombrecompleto_cliente' => 'Francisca Andres Pereira Hernandez',
            'fecha_registro' => '2016-05-17'
        ]);

        Cliente::create([
            'nro_sucursal' => 123,
            'nombrecompleto_cliente' => 'Ricardo Mardones Espinoza',
            'fecha_registro' => '2016-06-01'
        ]);

        Cliente::create([
            'nro_sucursal' => 124,
            'nombrecompleto_cliente' => 'Luis Sandoval Oliva',
            'fecha_registro' => '2016-06-01'
        ]);
    }
}
