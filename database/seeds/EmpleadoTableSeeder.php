<?php

use App\Empleado;
use Illuminate\Database\Seeder;

class EmpleadoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Empleado::create([
            'nro_sucursal' => 123,
            'nombre_empleado' => 'Esteban',
            'salario_empleado' => 2800,
            'cargo_empleado' => 'gerente'
        ]);

        Empleado::create([
            'nro_sucursal' => 123,
            'nombre_empleado' => 'Francisco',
            'salario_empleado' => 1000,
            'cargo_empleado' => 'reponedor'
        ]);

        Empleado::create([
            'nro_sucursal' => 123,
            'nombre_empleado' => 'Jorge',
            'salario_empleado' => 500,
            'cargo_empleado' => 'cajero'
        ]);

        Empleado::create([
            'nro_sucursal' => 124,
            'nombre_empleado' => 'Valentin',
            'salario_empleado' => 2800,
            'cargo_empleado' => 'gerente'
        ]);

        Empleado::create([
            'nro_sucursal' => 123,
            'nombre_empleado' => 'Felipe',
            'salario_empleado' => 1000,
            'cargo_empleado' => 'guardia'
        ]);

        Empleado::create([
            'nro_sucursal' => 123,
            'nombre_empleado' => 'Roberto',
            'salario_empleado' => 500,
            'cargo_empleado' => 'cajero'
        ]);

        Empleado::create([
            'nro_sucursal' => 125,
            'nombre_empleado' => 'Christofer',
            'salario_empleado' => 2800,
            'cargo_empleado' => 'gerente'
        ]);

        Empleado::create([
            'nro_sucursal' => 123,
            'nombre_empleado' => 'Mario',
            'salario_empleado' => 1000,
            'cargo_empleado' => 'guardia'
        ]);

        Empleado::create([
            'nro_sucursal' => 123,
            'nombre_empleado' => 'Mariana',
            'salario_empleado' => 500,
            'cargo_empleado' => 'supervisor'
        ]);

        Empleado::create([
            'nro_sucursal' => 123,
            'nombre_empleado' => 'Alicia',
            'salario_empleado' => 500,
            'cargo_empleado' => 'vendedor'
        ]);

        Empleado::create([
            'nro_sucursal' => 123,
            'nombre_empleado' => 'Camilo',
            'salario_empleado' => 2800,
            'cargo_empleado' => 'reponedor'
        ]);

        Empleado::create([
            'nro_sucursal' => 123,
            'nombre_empleado' => 'Daniel',
            'salario_empleado' => 1000,
            'cargo_empleado' => 'supervisor'
        ]);

        Empleado::create([
            'nro_sucursal' => 123,
            'nombre_empleado' => 'Matias',
            'salario_empleado' => 500,
            'cargo_empleado' => 'vendedor'
        ]);
    }
}
