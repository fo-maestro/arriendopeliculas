<?php

use App\Ciudad;
use Illuminate\Database\Seeder;

class CiudadTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Ciudad::create([
            'id_ciudad' => 'C001',
            'nombre_ciudad' => 'santiago',
            'nombre_estado' => 'metropolitana'
        ]);

        Ciudad::create([
            'id_ciudad' => 'C002',
            'nombre_ciudad' => 'concepcion',
            'nombre_estado' => 'bio-bio'
        ]);

        Ciudad::create([
            'id_ciudad' => 'C003',
            'nombre_ciudad' => 'Catalunya',
            'nombre_estado' => null
        ]);
    }
}
