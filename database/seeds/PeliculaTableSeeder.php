<?php

use App\Pelicula;
use Illuminate\Database\Seeder;

class PeliculaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Pelicula::create([
            'nro_catalogo' => 1111,
            'copias' => 40,
            'nro_pelicula' => 104,
            'titulo_pelicula' => 'Suki woman',
            'valor_diarioarriendo' => 100,
            'costo_pelicula' => 1190,
            'director_pelicula' => 'Juan',
            'categoria_pelicula' => 'adultos'
        ]);

        Pelicula::create([
            'nro_catalogo' => 1112,
            'copias' => 41,
            'nro_pelicula' => 105,
            'titulo_pelicula' => 'Rata salvaje',
            'valor_diarioarriendo' => 200,
            'costo_pelicula' => 2380,
            'director_pelicula' => 'Pedro',
            'categoria_pelicula' => 'terror'
        ]);

        Pelicula::create([
            'nro_catalogo' => 1113,
            'copias' => 0,
            'nro_pelicula' => 106,
            'titulo_pelicula' => 'El padratro',
            'valor_diarioarriendo' => 500,
            'costo_pelicula' => 1190,
            'director_pelicula' => 'Marcos',
            'categoria_pelicula' => 'adultos'
        ]);

        Pelicula::create([
            'nro_catalogo' => 1114,
            'copias' => 42,
            'nro_pelicula' => 107,
            'titulo_pelicula' => 'XXX',
            'valor_diarioarriendo' => 100,
            'costo_pelicula' => 1190,
            'director_pelicula' => 'Marcos',
            'categoria_pelicula' => 'adultos'
        ]);

        Pelicula::create([
            'nro_catalogo' => 1115,
            'copias' => 0,
            'nro_pelicula' => 108,
            'titulo_pelicula' => 'La casa embrujada',
            'valor_diarioarriendo' => 200,
            'costo_pelicula' => 2380,
            'director_pelicula' => 'Alejandro',
            'categoria_pelicula' => 'terror'
        ]);

        Pelicula::create([
            'nro_catalogo' => 1116,
            'copias' => 43,
            'nro_pelicula' => 109,
            'titulo_pelicula' => 'Nenem y Titim',
            'valor_diarioarriendo' => 500,
            'costo_pelicula' => 1700,
            'director_pelicula' => 'Valentin',
            'categoria_pelicula' => 'romantica'
        ]);

        Pelicula::create([
            'nro_catalogo' => 1117,
            'copias' => 44,
            'nro_pelicula' => 110,
            'titulo_pelicula' => 'Bambi',
            'valor_diarioarriendo' => 100,
            'costo_pelicula' => 1190,
            'director_pelicula' => 'Carla',
            'categoria_pelicula' => 'niños'
        ]);

        Pelicula::create([
            'nro_catalogo' => 1118,
            'copias' => 0,
            'nro_pelicula' => 111,
            'titulo_pelicula' => 'La vieja maldita',
            'valor_diarioarriendo' => 200,
            'costo_pelicula' => 2380,
            'director_pelicula' => 'Constanza',
            'categoria_pelicula' => 'terror'
        ]);

        Pelicula::create([
            'nro_catalogo' => 1119,
            'copias' => 0,
            'nro_pelicula' => 112,
            'titulo_pelicula' => 'Sashita el regreso',
            'valor_diarioarriendo' => 500,
            'costo_pelicula' => 1600,
            'director_pelicula' => 'Carla',
            'categoria_pelicula' => 'adultos'
        ]);
    }
}
