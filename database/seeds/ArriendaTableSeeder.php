<?php

use App\Cliente;
use App\CopiaPelicula;
use Illuminate\Database\Seeder;

class ArriendaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Cliente::find(1)->copia_pelicula()->attach('PE001', [
                'fecha_arriendo' => '2016-02-01', 'fecha_devolucion' => '2016-02-09'
            ]);

        Cliente::find(2)->copia_pelicula()->attach('PE004', [
            'fecha_arriendo' => '2016-03-03', 'fecha_devolucion' => '2016-03-10'
        ]);

        Cliente::find(3)->copia_pelicula()->attach('PE007', [
            'fecha_arriendo' => '2016-03-04', 'fecha_devolucion' => '2016-03-11'
        ]);

        Cliente::find(4)->copia_pelicula()->attach('PE004', [
            'fecha_arriendo' => '2016-04-11', 'fecha_devolucion' => '2016-04-17'
        ]);

        Cliente::find(5)->copia_pelicula()->attach('PE001', [
            'fecha_arriendo' => '2016-04-14', 'fecha_devolucion' => '2016-04-21'
        ]);

        Cliente::find(6)->copia_pelicula()->attach('PE007', [
            'fecha_arriendo' => '2016-05-17', 'fecha_devolucion' => '2016-05-24'
        ]);

        Cliente::find(7)->copia_pelicula()->attach('PE007', [
            'fecha_arriendo' => '2016-05-20', 'fecha_devolucion' => '2016-05-27'
        ]);

        Cliente::find(8)->copia_pelicula()->attach('PE001', [
            'fecha_arriendo' => '2016-06-01', 'fecha_devolucion' => '2016-06-08'
        ]);

        Cliente::find(9)->copia_pelicula()->attach('PE004', [
            'fecha_arriendo' => '2016-07-11', 'fecha_devolucion' => '2016-07-18'
        ]);
    }
}
