<?php

use App\Actor;
use Illuminate\Database\Seeder;

class ActorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Actor::create([
            'id_actor' => 'AC001',
            'nombre_actor' => 'Nabari'
        ]);

        Actor::create([
            'id_actor' => 'AC002',
            'nombre_actor' => 'Tambor'
        ]);

        Actor::create([
            'id_actor' => 'AC003',
            'nombre_actor' => 'Renton'
        ]);

        Actor::create([
            'id_actor' => 'AC004',
            'nombre_actor' => 'Belen'
        ]);

        Actor::create([
            'id_actor' => 'AC005',
            'nombre_actor' => 'Sasha'
        ]);

        Actor::create([
            'id_actor' => 'AC006',
            'nombre_actor' => 'Valentin'
        ]);
    }
}
