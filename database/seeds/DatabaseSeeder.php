<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(CiudadTableSeeder::class);
        $this->call(DireccionTableSeeder::class);
        $this->call(SucursalTableSeeder::class);
        $this->call(EmpleadoTableSeeder::class);
        $this->call(PeliculaTableSeeder::class);
        $this->call(CopiaPeliculaTableSeeder::class);
        $this->call(AlmacenaTableSeeder::class);
        $this->call(ActorTableSeeder::class);
        $this->call(ActuaTableSeeder::class);
        $this->call(ClienteTableSeeder::class);
        $this->call(ArriendaTableSeeder::class);

        Model::reguard();
    }
}
