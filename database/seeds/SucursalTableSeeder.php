<?php

use App\Sucursal;
use Illuminate\Database\Seeder;

class SucursalTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Sucursal::create([
            'nro_sucursal' => 123,
            'id_direccion' => 'D001',
            'nro_telefonicoSU' => 219444569,
            'nombre_sucursal' => 'the'
        ]);

        Sucursal::create([
            'nro_sucursal' => 124,
            'id_direccion' => 'D002',
            'nro_telefonicoSU' => 569289098,
            'nombre_sucursal' => 'legend'
        ]);

        Sucursal::create([
            'nro_sucursal' => 125,
            'id_direccion' => 'D003',
            'nro_telefonicoSU' => 569356346,
            'nombre_sucursal' => 'of zelda'
        ]);
    }
}
