<?php

use App\CopiaPelicula;
use Illuminate\Database\Seeder;

class CopiaPeliculaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CopiaPelicula::create([
            'correlativo' => 'PE001',
            'nro_catalogo' => 1111,
            'estado_copiapelicula' => 'disponible'
        ]);

        CopiaPelicula::create([
            'correlativo' => 'PE002',
            'nro_catalogo' => 1112,
            'estado_copiapelicula' => 'disponible'
        ]);

        CopiaPelicula::create([
            'correlativo' => 'PE003',
            'nro_catalogo' => 1113,
            'estado_copiapelicula' => 'no disponible'
        ]);

        CopiaPelicula::create([
            'correlativo' => 'PE004',
            'nro_catalogo' => 1114,
            'estado_copiapelicula' => 'disponible'
        ]);

        CopiaPelicula::create([
            'correlativo' => 'PE005',
            'nro_catalogo' => 1115,
            'estado_copiapelicula' => 'perdido'
        ]);

        CopiaPelicula::create([
            'correlativo' => 'PE006',
            'nro_catalogo' => 1116,
            'estado_copiapelicula' => 'disponible'
        ]);

        CopiaPelicula::create([
            'correlativo' => 'PE007',
            'nro_catalogo' => 1117,
            'estado_copiapelicula' => 'disponible'
        ]);

        CopiaPelicula::create([
            'correlativo' => 'PE008',
            'nro_catalogo' => 1118,
            'estado_copiapelicula' => 'perdido'
        ]);

        CopiaPelicula::create([
            'correlativo' => 'PE009',
            'nro_catalogo' => 1119,
            'estado_copiapelicula' => 'no disponible'
        ]);
    }
}
