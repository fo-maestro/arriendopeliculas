<?php

use App\Pelicula;
use App\Sucursal;
use Illuminate\Database\Seeder;

class AlmacenaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Sucursal::find(123)->pelicula()->save(Pelicula::find(1111));
        Sucursal::find(123)->pelicula()->save(Pelicula::find(1112));
        Sucursal::find(123)->pelicula()->save(Pelicula::find(1113));
        Sucursal::find(124)->pelicula()->save(Pelicula::find(1114));
        Sucursal::find(124)->pelicula()->save(Pelicula::find(1115));
        Sucursal::find(124)->pelicula()->save(Pelicula::find(1116));
        Sucursal::find(125)->pelicula()->save(Pelicula::find(1117));
        Sucursal::find(125)->pelicula()->save(Pelicula::find(1118));
        Sucursal::find(125)->pelicula()->save(Pelicula::find(1119));
    }
}
