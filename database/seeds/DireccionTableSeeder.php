<?php

use App\Ciudad;
use App\Direccion;
use Illuminate\Database\Seeder;

class DireccionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Direccion::create([
            'id_direccion' => 'D001',
            'id_ciudad' => 'C001',
            'calle' => 'mateo de toro y zambrano',
            'codigo_postal' => 832000
        ]);

        Direccion::create([
            'id_direccion' => 'D002',
            'id_ciudad' => 'C002',
            'calle' => 'san martin',
            'codigo_postal' => 4030000
        ]);

        Direccion::create([
            'id_direccion' => 'D003',
            'id_ciudad' => 'C003',
            'calle' => 'los canelos',
            'codigo_postal'=> 4190000
        ]);
    }
}
