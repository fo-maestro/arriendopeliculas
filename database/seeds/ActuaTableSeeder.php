<?php

use App\Actor;
use App\Pelicula;
use Illuminate\Database\Seeder;

class ActuaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Pelicula::find(1111)->actor()->save(Actor::find('AC001'));
        Pelicula::find(1117)->actor()->save(Actor::find('AC002'));
        Pelicula::find(1111)->actor()->save(Actor::find('AC003'));
        Pelicula::find(1116)->actor()->save(Actor::find('AC004'));
        Pelicula::find(1111)->actor()->save(Actor::find('AC005'));
        Pelicula::find(1112)->actor()->save(Actor::find('AC003'));
        Pelicula::find(1116)->actor()->save(Actor::find('AC006'));
        Pelicula::find(1113)->actor()->save(Actor::find('AC006'));
        Pelicula::find(1118)->actor()->save(Actor::find('AC001'));
        Pelicula::find(1119)->actor()->save(Actor::find('AC003'));
        Pelicula::find(1115)->actor()->save(Actor::find('AC006'));
        Pelicula::find(1119)->actor()->save(Actor::find('AC005'));
        Pelicula::find(1114)->actor()->save(Actor::find('AC005'));
    }
}
