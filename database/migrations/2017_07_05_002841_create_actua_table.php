<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActuaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actua', function (Blueprint $table) {
            $table->integer('nro_catalogo');
            $table->string('id_actor', 10);
            $table->foreign('nro_catalogo')->references('nro_catalogo')->on('pelicula');
            $table->foreign('id_actor')->references('id_actor')->on('actor');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('actua');
    }
}
