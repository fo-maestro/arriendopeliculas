<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpleadoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empleado', function (Blueprint $table) {
            $table->increments('nro_empleado');
            $table->integer('nro_sucursal');
            $table->string('nombre_empleado', 50);
            $table->integer('salario_empleado');
            $table->string('cargo_empleado', 50);
            $table->foreign('nro_sucursal')->references('nro_sucursal')->on('sucursal');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('empleado');
    }
}
