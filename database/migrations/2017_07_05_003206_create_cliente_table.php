<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClienteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cliente', function (Blueprint $table) {
            $table->increments('nro_cliente');
            $table->integer('nro_sucursal');
            $table->date('fecha_registro');
            $table->string('nombrecompleto_cliente', 50);
            $table->foreign('nro_sucursal')->references('nro_sucursal')->on('sucursal');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cliente');
    }
}
