<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCopiaPeliculaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('copia_pelicula', function (Blueprint $table) {
            $table->string('correlativo', 10);
            $table->integer('nro_catalogo');
            $table->string('estado_copiapelicula', 50);
            $table->primary('correlativo');
            $table->foreign('nro_catalogo')->references('nro_catalogo')->on('pelicula');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('copia_pelicula');
    }
}
