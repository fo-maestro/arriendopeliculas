<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArriendaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('arrienda', function (Blueprint $table) {
            $table->increments('nro_arriendo');
            $table->unsignedinteger('nro_cliente');
            $table->string('correlativo', 10);
            $table->date('fecha_arriendo');
            $table->date('fecha_devolucion');
            $table->foreign('nro_cliente')->references('nro_cliente')->on('cliente');
            $table->foreign('correlativo')->references('correlativo')->on('copia_pelicula');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('arrienda');
    }
}
