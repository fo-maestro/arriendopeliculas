<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateConsultaView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement(
            'CREATE VIEW consulta AS 
            (SELECT cl.nro_cliente AS codigo_cliente, cl.nombrecompleto_cliente AS nombre_cliente, count(*) AS cantidad
            FROM cliente cl, arrienda a
            WHERE cl.nro_cliente = a.nro_cliente
            GROUP BY cl.nro_cliente, cl.nombrecompleto_cliente
            HAVING count(*) > 0)'
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS consulta');
    }
}
