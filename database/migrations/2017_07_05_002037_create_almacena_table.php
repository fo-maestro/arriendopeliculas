<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlmacenaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('almacena', function (Blueprint $table) {
            $table->integer('nro_sucursal');
            $table->integer('nro_catalogo');
            $table->foreign('nro_sucursal')->references('nro_sucursal')->on('sucursal');
            $table->foreign('nro_catalogo')->references('nro_catalogo')->on('pelicula');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('almacena');
    }
}
