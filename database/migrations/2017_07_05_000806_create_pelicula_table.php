<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeliculaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pelicula', function (Blueprint $table) {
            $table->integer('nro_catalogo');
            $table->integer('copias');
            $table->integer('nro_pelicula');
            $table->string('titulo_pelicula', 50);
            $table->integer('valor_diarioarriendo');
            $table->integer('costo_pelicula');
            $table->string('director_pelicula', 50);
            $table->string('categoria_pelicula', 50);
            $table->primary('nro_catalogo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pelicula');
    }
}
