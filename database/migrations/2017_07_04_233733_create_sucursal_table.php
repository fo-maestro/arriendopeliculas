<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSucursalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sucursal', function (Blueprint $table) {
            $table->integer('nro_sucursal');
            $table->string('id_direccion', 15);
            $table->integer('nro_telefonicoSU');
            $table->string('nombre_sucursal', 50);
            $table->primary('nro_sucursal');
            $table->foreign('id_direccion')->references('id_direccion')->on('direccion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sucursal');
    }
}
